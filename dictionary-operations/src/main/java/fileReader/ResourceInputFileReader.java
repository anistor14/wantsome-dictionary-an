package fileReader;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ResourceInputFileReader {

    public List<String> readFile(String input) throws IOException {
        //lista unde salvam fiecare cuv din fiserul dex.txt
        List<String> result = new ArrayList<>();

        // se formeaza calea unde este fisierul nostru
        ClassLoader classLoader = ResourceInputFileReader.class.getClassLoader();
        URL resource = classLoader.getResource(input);
        File file;

        // tratat daca nu gaseste fisierul apare eraore, daca il gaseste il ia
        if(resource == null){
            throw new IllegalArgumentException("file not found!");
        } else{
            file = new File(resource.getFile());
        }

        // citim  fisierul (linie cu linie din fiser)
        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)){

            //linie cu linie ;)
            String line;
            while((line = br.readLine()) != null){
                result.add(line); // adaugam fiecare linie in lista noastra
            }
        }

        return result; // ni se returneaza lista cu cuvinte
    }
}
