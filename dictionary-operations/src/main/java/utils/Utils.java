package utils;


import java.util.*;
import java.util.stream.Collectors;

public class Utils { // metode pe care le vom folosi ptr date

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordsSet, String userInput) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            if (line.contains(userInput)) {
                result.add(line);
            }
        }
        return result;
    }

    public static Set<String> findAllPalindromes(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }
        return result;
    }

    public static String sortLettersInWord(String word) { //sorts all the letters in a word alphabetically.
        char[] a = word.toCharArray();
        Arrays.sort(a);
        return new String(a);
    }

    public static Map<String, List<String>> anagramsMapFinder(Set<String> wordsSet) {
        Map<String, List<String>> anagrams = new HashMap<>();
        //String[] wordsArray = wordsSet.toArray(new String[0]);

        for (String value : wordsSet) {  //if it's used Array above then instead of wordsSet should be wordsArray (both methods works fine)
            String word = String.valueOf(value);
            String sortedWord = sortLettersInWord(word); //sort the letters in the word

            if (anagrams.containsKey(sortedWord)) { //If the map has a key with this sorted word, add the original word to the value list
                anagrams.get(sortedWord).add(word);
            } else { //If the map doesn't have this key, create a new list, add the word to it, and add it to the map
                List<String> words = new ArrayList<>();
                words.add(word);
                anagrams.put(sortedWord, words);
            }
        }
        return anagrams;
    }
}
