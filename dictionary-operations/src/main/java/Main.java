import dictionaryOperations.*;
import fileReader.ResourceInputFileReader;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("This is my main class.");

        // citim de la tastatura (urmatoarele 2 linii de cod)
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); //ofera posibilitatea de a citi de la tastatura
        //       String userInput = in.readLine(); // linia ptr a introduce textul

        List<String> words = new ArrayList<>(); // facem o lista
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect
        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru
        Set<String> wordsNoDuplicates = Utils.removeDuplicates(words);

        while (true) {
            System.out.println("Menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes \n" +
                    "3. Anagrams: \n" +
                    "\t 3.1 All groups of anagrams \n" +
                    "\t 3.2 Anagrams for a given word \n" +
                    "0. Exit");

            String userInput = in.readLine();
            if (userInput.equals("0")) {
                System.out.println("\n===== Have a nice day!!! ===== \n");
                break;
            }

            //folosim switch pentru a naviga in meniu
            DictionaryOperation operation = null;
            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsNoDuplicates, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsNoDuplicates);
                    break;
                case "3":
                    System.out.println("\nPlease select what you want related to anagrams (3.1 or 3.2)! \n");
                    break;
                case "3.1":
                    operation = new AnagramsSearchAllGroups(wordsNoDuplicates);
                    break;
                case "3.2":
                    operation = new AnagramsSearchAfterWord(wordsNoDuplicates, in);
                    break;
                default:
                    System.out.println("\nInvalid option!\n");
            }

            if (operation != null) {
                operation.run();
            }
        }

//        System.out.println("User input: " + userInput);
//        userInput = in.readLine();
//        System.out.println("Second input: " + userInput);

//        List<String> words = new ArrayList<>(); // facem o lista
//        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect
//        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru

//        for (String word : words) {
//            System.out.println(word);
//        }

//        Set<String> wordsNoDuplicates = utils.Utils.removeDuplicates(words);
//        for (String word : wordsNoDuplicates) {
//            System.out.println(word);
//        }
    }
}