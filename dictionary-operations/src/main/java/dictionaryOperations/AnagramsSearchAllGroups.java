package dictionaryOperations;

import utils.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

//  find all group of anagrams from the dictionary
public class AnagramsSearchAllGroups implements DictionaryOperation {
    private Set<String> wordsSet;

    public AnagramsSearchAllGroups(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("\n===== All groups of Anagrams =====\n");

        Map<String, List<String>> anagrams = Utils.anagramsMapFinder(wordsSet);

        for (String s : anagrams.keySet()) {
            List<String> values = anagrams.get(s);
            if (values.size() > 1) {
                System.out.println(values.toString());
            }
        }

        System.out.println("\n===== Finished display all groups of anagrams! =====\n");
    }
}