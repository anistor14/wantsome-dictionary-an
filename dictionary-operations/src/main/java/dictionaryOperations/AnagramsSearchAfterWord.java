package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

//  find all the anagrams of a given word in the dictionary
public class AnagramsSearchAfterWord implements DictionaryOperation {
    private Set<String> wordsSet;
    private BufferedReader in;

    public AnagramsSearchAfterWord(Set<String> wordsSet, BufferedReader in) {
        this.in = in;
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("\n===== Enter the word you want to search! =====\n");

        Map<String, List<String>> anagrams = Utils.anagramsMapFinder(wordsSet);
        String userInput = in.readLine();
        String userInputSorted = Utils.sortLettersInWord(userInput);

        if (anagrams.containsKey(userInputSorted.toLowerCase())) {
            List<String> userInputAnagrams = anagrams.get(userInputSorted.toLowerCase());
            System.out.println("\nAll anagrams for word \"" + userInput + "\" are: " + userInputAnagrams);
        } else {
            System.out.println("\nNo anagrams for given word: \"" + userInput + "\".");
        }

        System.out.println("\n===== The display of all anagrams for the word \"" + userInput.toUpperCase() + "\" was completed successfully! =====\n");
    }
}