package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;

//find all palindromes from dictionary
public class PalindromeDictionaryOperation implements DictionaryOperation {

    private Set<String> wordsSet;

    public PalindromeDictionaryOperation(Set<String> wordsSet){

        this.wordsSet=wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("\n===== Palindromes =====\n");
        Set<String> palindromes = Utils.findAllPalindromes(wordsSet);

        for (String line: palindromes){
            System.out.println(line);
        }
        System.out.println("\n===== Finished! =====\n");

    }
}
