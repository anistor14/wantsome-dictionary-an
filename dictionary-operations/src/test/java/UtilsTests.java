import fileReader.ResourceInputFileReader;
import org.junit.Test;
import utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilsTests {

    @Test
    public void testPalindromeFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("ana");
        Set<String> palindromes = Utils.findAllPalindromes(inputSet);
        assertTrue(palindromes.contains("unu"));
        assertEquals(1, palindromes.size());

    }

    @Test
    public void testPalindromeFromFile() throws IOException {
        List<String> words = new ArrayList<>(); // facem o lista
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect
        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru
        Set<String> wordsNoDuplicates = Utils.removeDuplicates(words);

        Set<String> palindromes = Utils.findAllPalindromes(wordsNoDuplicates);

        assertTrue(palindromes.contains("aba"));
        assertTrue(palindromes.size() == 3);
    }

    @Test
    public void testPalindromesNoResult() throws IOException {

        Set<String> inputSet = new HashSet<>();
        inputSet.add("telefon");

        Set<String> palindromes = Utils.findAllPalindromes(inputSet);
        assertTrue(palindromes.size() == 0);
    }
}
