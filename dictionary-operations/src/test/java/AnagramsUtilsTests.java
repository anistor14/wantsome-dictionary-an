import fileReader.ResourceInputFileReader;
import org.junit.Test;
import utils.Utils;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class AnagramsUtilsTests {

    @Test
    public void testSortLettersInWord() {
        String word = "test";
        String wordSorted = Utils.sortLettersInWord(word);

        assertEquals("estt", wordSorted);
        assertTrue(wordSorted.startsWith("e"));
        assertTrue(wordSorted.endsWith("t"));
    }

    @Test
    public void testAnagramsFinder() {
        Set<String> words = new HashSet<>();
        words.add("sau");
        words.add("asu");
        words.add("cuvant");
        words.add("iaz");
        words.add("azi");
        Map<String, List<String>> anagrams = Utils.anagramsMapFinder(words);

        assertTrue(anagrams.containsKey("aiz"));
        assertEquals(3, anagrams.size());
        assertTrue(anagrams.values().toString().contains("[asu, sau]"));
    }

    @Test
    public void testAnagramsFinderFromFile() throws IOException {
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        List<String> words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicates = Utils.removeDuplicates(words);

        Map<String, List<String>> anagrams = Utils.anagramsMapFinder(wordsNoDuplicates);

        assertTrue(anagrams.containsKey("aiz"));
        assertTrue(anagrams.size() > 20);
        assertTrue(anagrams.values().toString().contains("[mezi, miez]"));
    }
}